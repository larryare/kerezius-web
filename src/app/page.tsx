"use client";
import Image from "next/image";
import localFont from "next/font/local"
import { Button } from "@/components/ui/button"
import { Mail, ScrollText, ArrowRight } from "lucide-react"
import Link from "next/link";
import { AuroraBackground } from "@/components/ui/aurora-background";
import { motion } from "framer-motion";
import { ShimmerButton } from "@/components/ui/shimmer-button";
import { useToast } from "@/components/ui/use-toast"


const appleFont = localFont({ src: "./Bumbbled.otf" })


export default function Home() {
  const { toast } = useToast()
  return (
    <AuroraBackground>
      <motion.div
        initial={{ opacity: 0.0, y: 40 }}
        whileInView={{ opacity: 1, y: 0 }}
        transition={{
          delay: 0.3,
          duration: 0.8,
          ease: "easeInOut",
        }}
        className="relative flex flex-col gap-4 items-center justify-center px-4"
      >
        <main className="flex flex-col h-screen items-center justify-center">
          <div className='text-transparent bg-gradient-to-r from-pink-500 to-yellow-300 bg-clip-text px-5 py-9 my-20'>
            <h1 className={`text-6xl md:text-9xl + ${appleFont.className}`}>L. Kerežius</h1>
          </div>
          <div className="flex flex-row">

            <Link href={"https://benzas.lt"}>
              <Button variant="outline">
                {/* <ScrollText className="mr-2 h-4 w-4" /> */}
                <Image src={"/hip-replacement.png"} height={20} width={20} alt="Protezas" className="mr-2"></Image>
                CV</Button>
            </Link>

            <Link href={"mailto:laurynas@kerezius.lt"}>
              <Button variant="outline" className="mx-2">
                <Mail className="mr-2 h-4 w-4" /> Email
              </Button>
            </Link>
            {/* <Link href={"#"}>
              <Button variant="outline" onClick={() => {
                toast({
                  title: "Dar negalima!",
                  description: "Vyksta konstrukcija 🚧",
                })
              }}>
                <Image src={"/hip-replacement.png"} height={20} width={20} alt="Protezas" className="mr-2"></Image>
                Rezidentūrai
              </Button>
            </Link> */}
          </div>
        </main >
      </motion.div>
    </AuroraBackground>
  );
}
