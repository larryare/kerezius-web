'use client';
import { AuroraBackground } from "@/components/ui/aurora-background";
import { motion } from "framer-motion";
import Link from "next/link";
import { ShimmerButton } from "@/components/ui/shimmer-button";
import { useEffect } from "react";
import { CardBody, CardContainer, CardItem } from "@/components/ui/3d-card";

export default function Rugiliukui() {
    useEffect(() => {
        document.title = "👉 👈 🥹";
    }, []);
    return (

        <AuroraBackground>
            <motion.div
                initial={{ opacity: 0.0, y: 40 }}
                whileInView={{ opacity: 1, y: 0 }}
                transition={{
                    delay: 0.3,
                    duration: 0.8,
                    ease: "easeInOut",
                }}
                className="relative flex flex-col gap-4 items-center justify-center px-4"
            >
                <CardContainer>
                    <CardBody className="relative group/card flex flex-col h-96 w-96 bg-gradient-to-br from-pink-200 to-pink-800 rounded-3xl shadow-2xl p-3">
                        <div className="p-2 text-center items-center justify-center w-full flex flex-col">
                            <CardItem as={"h1"} translateZ="30" className=" text-white text-3xl font-bold [text-shadow:_0_1px_0_rgb(0_0_0_/_40%)]">
                                Kvietimas
                            </CardItem>
                            <CardItem as="p" translateZ="70" className="">
                                👉 👈 🥹
                            </CardItem>
                        </div>
                        <div className="flex flex-row space-x-1 w-full h-full">
                            <div className="h-full w-full flex flex-col">
                                <CardItem translateZ={100} translateX={"-20"} translateY="-10" className="w-auto h-full bg-gradient-to-br from-indigo-100 to-indigo-800 drop-shadow-lg rounded-2xl m-2 p-2">
                                    <h1 className="text-center text-xl font-bold">Lokacija</h1>
                                    <p>Kaunas city***</p>
                                </CardItem>
                                <CardItem translateZ={100} translateX={"-20"} translateY={"10"} className="w-auto h-full bg-gradient-to-br from-amber-100 to-amber-800 drop-shadow-lg rounded-2xl m-2 p-2">
                                    <h1 className="text-center text-xl font-bold">Data ir laikas</h1>
                                    <p>Gegužės 11****</p>
                                    <p className="italic text-sm">(flexible)</p>
                                </CardItem>
                            </div>
                            <CardItem translateZ={100} translateX={"20"} className="bg-gradient-to-br from-emerald-100 to-emerald-800 drop-shadow-lg h-full w-full p-2 rounded-2xl hover:drop-shadow-2xl">
                                <h1 className="text-center font-bold text-2xl">Programa</h1>
                                <p>❗️ À La Carte meniu (incl. šaltibai)</p>
                                <p>❗️ Eurovizijos finalas 2024</p>
                                <p>❓ Ice skating</p>
                                <p>❓ Kauno moto turas*</p>
                                <p>❓ SPA</p>
                                <p>❗️ {">"}50 free indoor activities**</p>
                            </CardItem>
                        </div>
                    </CardBody>
                </CardContainer>
                <div className="text-xs">
                    <span>
                        *Vietų kiekis ribotas
                    </span>
                    <br />
                    <span>
                        **Meninės (galimai) dirbtuvės gegužės 11 dienai paminėti
                    </span>
                    <br />
                    <span>
                        ***Pickup and dropoff available on request (kontaktinis tel. nr. <Link href="tel:+37060975842"><span className="underline italic">+37060975842</span></Link>)
                    </span>
                    <br />
                    <span>
                        ****Nemokamas BNB available
                    </span>
                </div>
                <Link href="https://www.instagram.com/pasiuskreelsiuku/">
                    <ShimmerButton></ShimmerButton>
                </Link>

            </motion.div >
        </AuroraBackground >

    )
}