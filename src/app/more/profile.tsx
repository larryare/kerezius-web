import Image from "next/image"
import { Badge } from "@/components/ui/badge"
import { Button } from "@/components/ui/button"
import localFont from "next/font/local"
const appleFont = localFont({ src: "../Bumbbled.otf" })



export function Profile() {
    return (
        <div className="flex flex-col justify-between">
            <div className="flex flex-row">
                <Image
                    src="/profiline.png"
                    alt="avatar"
                    height="200"
                    width="200"
                    className="rounded-full h-20 w-20"
                />
                <div className="flex flex-col w-full h-full bg-red-200 items-center justify-center m-5">
                    <h1 className={`text-md md:text-lg  bg-slate-400 + ${appleFont.className}`}>Laurynas Kerežius</h1>
                </div>
            </div>
            <div>
                {/* <Badge variant="outline">Anglų, lietuvių</Badge> */}

            </div>
        </div>
    )
}