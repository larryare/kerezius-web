FROM --platform=linux/arm64 node:latest 
WORKDIR /app

COPY . .

RUN npm install

RUN npm run build

EXPOSE 3000

ENV PORT 3000

CMD ["npm", "run", "start"]

